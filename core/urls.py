from django.urls import path

from core.views import CreateTask, Homepage, Info, Students, Student, Task, Tasks


urlpatterns = [
    path('', Homepage.as_view(), name='homepage'),
    path('info/', Info.as_view(), name='info'),
    path('students/', Students.as_view(), name='students'),
    path('student/<id>/', Student.as_view(), name='student'),
    path('tasks/', Tasks.as_view(), name='tasks'),
    path('task/<id>/', Task.as_view(), name='task'),
    path('tasks/create/', CreateTask.as_view(), name='create_task'),
]
