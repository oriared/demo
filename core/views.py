import datetime

from django.db.models import Count, Q
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views import View
from django.views.generic import ListView, DetailView, TemplateView, FormView, CreateView

from core import models, forms


class Homepage(TemplateView):
    template_name = 'core/mainpage.html'
    extra_context = {'title': 'Главная', 'text': 'Приветствую Вас!'}


class Info(TemplateView):
    template_name = 'core/info.html'
    extra_context = {'title': 'О проекте'}


class Students(ListView):
    template_name = 'core/students.html'
    extra_context = {'title': 'Студенты'}
    context_object_name = 'students'

    def get_queryset(self):
        queryset = models.Student.objects.filter(is_active=True)

        sort_param = self.request.GET.get('sort_by')
        if sort_param == 'last_name':
            queryset = queryset.order_by('last_name')
        elif sort_param == 'tasks':
            queryset = queryset.annotate(tasks_quantity=Count('tasks')).order_by('-tasks_quantity')

        search_name = self.request.POST.get('search_name')
        if search_name:
            queryset = queryset.filter(
                Q(last_name__contains=search_name) |
                Q(first_name__contains=search_name) |
                Q(middle_name__contains=search_name)
            )

        return queryset


class Student(DetailView):
    template_name = 'core/student.html'
    model = models.Student
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        student = self.object
        name = student.last_name
        tasks = student.tasks.order_by('-deadline')
        return super().get_context_data(title=name, tasks=tasks)


class Tasks(ListView):
    template_name = 'core/tasks.html'
    extra_context = {'title': 'Задачи'}
    context_object_name = 'tasks'
    queryset = models.Task.objects.order_by('-dc')


class Task(DetailView):
    template_name = 'core/task.html'
    model = models.Task
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        task = self.object
        return super().get_context_data(title=task.title)


# class CreateTask(View):
#     def get(self, request, *args, **kwargs):
#         return render(request, template_name='core/create_task.html', context={'title': 'Новая задача'})
#
#     def post(self, request, *args, **kwargs):
#         title = request.POST.get('title')
#         description = request.POST.get('description')
#         deadline = request.POST.get('deadline')
#         if title:
#             task = models.Task(title=title)
#             if description:
#                 task.description = description
#             if deadline:
#                 deadline = datetime.datetime.strptime(deadline, '%Y-%m-%d').date()
#                 if deadline < datetime.date.today():
#                     return self.get(request)
#                 task.deadline = deadline
#             task.save()
#         return redirect('tasks')


# class CreateTask(View):
#     def get(self, request, *args, **kwargs):
#         form = kwargs.get('form') or forms.Task()
#         return render(request, template_name='core/create_task.html',
#                       context={'title': 'Новая задача', 'form': form})
#
#     def post(self, request, *args, **kwargs):
#         form = forms.Task(request.POST)
#         if form.is_valid():
#             title = form.cleaned_data.get('title')
#             description = form.cleaned_data.get('description')
#             deadline = form.cleaned_data.get('deadline')
#             task = models.Task(title=title)
#             if description:
#                 task.description = description
#             if deadline:
#                 task.deadline = deadline
#             task.save()
#             return redirect('tasks')
#         else:
#             return self.get(request, form=form)


# class CreateTask(FormView):
#     template_name = 'core/create_task.html'
#     form_class = forms.Task
#     success_url = reverse_lazy('tasks')
#
#     def form_valid(self, form):
#         form.save()
#         return redirect(self.get_success_url())


class CreateTask(CreateView):
    model = models.Task
    fields = ('title', 'description', 'deadline', 'students')
    template_name = 'core/create_task.html'
    success_url = reverse_lazy('tasks')
