import datetime

from django import forms

from core import models


# class Task(forms.Form):
#     title = forms.CharField(label='Название', min_length=5)
#     description = forms.CharField(label='Описание', required=False, widget=forms.Textarea)
#     deadline = forms.DateField(label='Дедлайн', required=False)
#
#     def clean_deadline(self):
#         deadline = self.cleaned_data['deadline']
#         if deadline and deadline < datetime.date.today():
#             raise forms.ValidationError('Дедлайн не может быть в прошлом!')
#         return self.cleaned_data['deadline']
#
#     def clean(self):
#         if self.cleaned_data['title'] == self.cleaned_data['description']:
#             raise forms.ValidationError('Название и описание не должны совпадать!')
#         return self.cleaned_data


# class Task(forms.ModelForm):
#     class Meta:
#         model = models.Task
#         fields = ('title', 'description', 'deadline', 'students')
#
#     def clean_deadline(self):
#         deadline = self.cleaned_data['deadline']
#         if deadline and deadline < datetime.date.today():
#             raise forms.ValidationError('Дедлайн не может быть в прошлом!')
#         return self.cleaned_data['deadline']
#
#     def clean(self):
#         if self.cleaned_data['title'] == self.cleaned_data['description']:
#             raise forms.ValidationError('Название и описание не должны совпадать!')
#         return self.cleaned_data
