import datetime

from django.contrib.auth import get_user_model
from django.db import models


User = get_user_model()


class Student(models.Model):
    first_name = models.CharField('имя', max_length=50, blank=True)
    last_name = models.CharField('фамилия', max_length=50, blank=True)
    middle_name = models.CharField('отчество', max_length=50, blank=True)
    contact = models.CharField('контакт', max_length=255, blank=True)
    is_active = models.BooleanField('активен', default=True)
    dc = models.DateTimeField('создан', auto_now_add=True)

    def __str__(self):
        return self.get_full_name_str()

    def get_full_name_str(self) -> str:
        last_name = self.last_name
        first_name = self.first_name
        middle_name = self.middle_name

        if first_name:
            return f'{last_name} {first_name} {middle_name}'.strip()
        elif last_name:
            return last_name
        else:
            return 'Нет данных'

    def get_out_of_deadline_tasks(self):
        return self.tasks.filter(deadline__lt=datetime.date.today())


class Project(models.Model):
    student = models.ForeignKey(Student, verbose_name='студент', on_delete=models.PROTECT, related_name='projects')
    url = models.URLField('URL')
    dc = models.DateTimeField('создан', auto_now_add=True)

    def __str__(self):
        return f'Проект студента {str(self.student)}'


class Task(models.Model):
    students = models.ManyToManyField(Student, verbose_name='студенты', related_name='tasks', blank=True)
    projects = models.ManyToManyField(Project, verbose_name='проекты', related_name='tasks')
    title = models.CharField('название', max_length=255)
    description = models.TextField('описание', blank=True)
    creator = models.ForeignKey(User, verbose_name='кто создал задачу', related_name='tasks',
                                on_delete=models.SET_NULL, null=True)
    deadline = models.DateField('дедлайн', null=True, blank=True)
    dc = models.DateTimeField('создан', auto_now_add=True)

    def __str__(self):
        return self.title
